// Copyright 2001-2005, Martin Str�mberg.
//-----------------------------------------------------------------------------
// Prints null-terminated string pointed to by ax through INT10, AH=0xe.
// In:	AX - pointer to string to print.
// Out:	AX - destroyed.

print_string:
	push	%bp
	push	%di
	push	%bx
	push	%ds
	mov	%ax, %di
	mov	%cs, %ax
	mov	%ax, %ds
	mov	$0xe, %ah
	mov	$0x0070, %bx
1:	//@@loop:	
	mov	(%di), %al
	or	%al, %al
	jz	2f //@@epilogue
	int	$0x10
	inc	%di
	jmp	1b //@@loop
2:	//@@epilogue:	
	pop	%ds
	pop	%bx
	pop	%di
	pop	%bp
	ret
