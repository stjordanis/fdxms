// Copyright 2001-2005, Martin Str�mberg.
//----------------------------------------------------------------------------
// returns free XMS
// In:	AH=8
// Out:	AX=size of largest free XMS block in kbytes
//	DX=total amount of free XMS in kbytes
//	BL=0 if ok
//	BL=080h -> function not implemented
//	BL=081h -> VDISK is detected
//	BL=0a0h -> all XMS is allocated

xms_query_free_xms:
	call	query_free_xms
	mov	%bl, C_BL

	// Limit EDX to 0xfbc0 KiB.
	cmp	$XMS_MAX, %edx
	jb	2f //@@dx_ok

	mov	$XMS_MAX, %dx
2:	//@@dx_ok:
	mov	%dx, C_DX

	// Limit EAX to 0xfbc0 KiB.
	cmp	$XMS_MAX, %eax
	jb	3f //@@ax_ok

	mov	$XMS_MAX, %ax
3:	//@@ax_ok:
	mov	%ax, C_AX
	ret

//-----------------------------------------------------------------------------
// allocates an XMS block
// In:	AH=9
//	DX=amount of XMS being requested in kbytes
// Out:	AX=1 if successful
//	  DX=handle
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> no free handles left

xms_alloc_xms:
	push	%dx
	xor	%edx, %edx
	pop	%dx
	
	call	alloc_xms
	jc	1f // Failure.
	ret
1:
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// returns XMS handle information
// In:	AH=0eh
//	DX=XMS block handle
// Out:	AX=1 if successful
//	  BH=block's lock count
//	  BL=number of free XMS handles
//	  DX=block's length in kbytes
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid

xms_get_handle_info:
	call	get_handle_info
	jc	4f // Failure.

	cmp	$XMS_MAX, %edx		// Is EDX to big?
	ja	3f //@@failure_edx
	
	mov	%dx, C_DX
	
	cmp	$0xff, %cx
	jbe	2f //@@cx_not_too_big
	mov	$0xff, %cl
2: 	//@@cx_not_too_big:	
	mov	%cl, %bl
	mov	%bx, C_BX
	ret

3: 	//@@failure_edx:
	mov	$XMS_INVALID_HANDLE, %bl
	
4:	//@@failure:
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// reallocates an XMS block. only supports shrinking.
// In:	AH=0fh
//	BX=new size for the XMS block in kbytes
//	DX=unlocked XMS handle
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> all handles are in use
//	  BL=0a2h -> invalid handle
//	  BL=0abh -> block is locked

xms_realloc_xms:
	push	%bx
	xor	%ebx, %ebx
	pop	%bx

	call	realloc_xms
	jc	1f // Failure
	ret

1:	// Failure.
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// returns free (X)XMS
// In:	-
// Out:	EAX=size of largest free XMS block in KiB
//	ECX=highest ending address of any memory block
//	EDX=total amount of free XMS in KiB
//	BL=0 if ok
//	BL=080h -> function not implemented
//	BL=081h -> VDISK is detected
//	BL=0a0h -> all XMS is allocated
// Destroys BH
// Destroys ESI

query_free_xms:
	mov	xms_num_handles, %cx
	mov	$driver_end, %bx
	
	xor	%esi, %esi		// Contains largest free block.
	xor	%edx, %edx		// Contains total free XMS.
	
1:	//@@check_next:
	cmpb	$0, xms_handle_used(%bx)
	jne	2f //@@in_use

	mov	xms_handle_xsize(%bx), %eax	// Get size.
	add	%eax, %edx
	cmp	%esi, %eax		// Is this block bigger than what we've found so far?
	jbe	2f //@@not_larger
	
	mov	%eax, %esi		// Larger, update.
	
2: 	//@@in_use:
	//@@not_larger:
	add	$SIZE_xms_handle, %bx
	loop	1b //@@check_next

	mov	%esi, %eax		// Move largest free size into EAX.
	
	mov	xms_highest_address, %ecx
	
	test	%eax, %eax		// Is there any free memory?
	jz	3f //@@no_free_xms
	
	xor	%bl, %bl
	ret

3: 	//@@no_free_xms:
	mov	$XMS_ALREADY_ALLOCATED, %bl
	ret

//-----------------------------------------------------------------------------
// returns free (X)XMS
// In:	AH=88h
// Out:	EAX=size of largest free XMS block in KiB
//	ECX=highest ending address of any memory block
//	EDX=total amount of free XMS in KiB
//	BL=0 if ok
//	BL=080h -> function not implemented
//	BL=081h -> VDISK is detected
//	BL=0a0h -> all XMS is allocated

xms_query_any_free_xms:
	call	query_free_xms

	mov	%eax, C_EAX
	mov	%ecx, C_ECX
	mov	%edx, C_EDX
	mov	%bl, C_BL

	ret

//-----------------------------------------------------------------------------
// allocates an (X)XMS block
// In:	EDX=amount of XMS being requested in kbytes
// Out:	CY=0 if successful
//	  C_DX=handle
//	CY=1 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> no free handles left

alloc_xms:
	call	xms_find_free_block	// See if there's a free block.
	jc	5f //@@no_free_memory	// If there isn't, fail.
	jmp	2f //@@check_size
	
1: 	//@@get_next_block:
	call	xms_find_next_free_block
	jc	5f //@@no_free_memory
2: 	//@@check_size:
	mov	xms_handle_xsize(%bx), %eax	// Check if it's large enough.
	cmp	%eax, %edx
	ja	1b //@@get_next_block	// No, get next block.

	mov	%bx, %si		// Save handle.
	incb	xms_handle_used(%bx)	// This block is used from now on.

3: 	//@@find_handle:	
	call	xms_find_free_handle	// see if there's a blank handle
	jc	4f //@@perfect_fit	// no, there isn't, alloc all mem left

	sub	%edx, %eax		// Calculate remaining memory.
	jz	4f //@@perfect_fit	// if it fits perfectly, go on
	
	mov	%eax, xms_handle_xsize(%bx)	// Store sizes of new blocks.
	mov	%edx, xms_handle_xsize(%si)
	mov	xms_handle_xbase(%si), %eax	// Get base address of old block.
	add	%edx, %eax		// Calculate new base address.
	mov	%eax, xms_handle_xbase(%bx)	// Store it in new handle.

4:	//@@perfect_fit:

	// Success.
	mov	%si, C_DX
	clc
	ret
	
5:	//@@no_free_memory:
	// If no memory was asked for, just allocate a handle.
	test	%edx, %edx
	jz	6f //@@zero_size_allocation
	
	mov	$XMS_ALREADY_ALLOCATED, %bl
	jmp	8f //@@failure_epilogue

6:	//@@zero_size_allocation:
	call	xms_find_free_handle	// see if there's a blank handle
	jc	7f //@@no_handles_left	// No, there isn't, fail.

	mov	%bx, %si		// Save handle address.
	
	// We have the handle. Mark it as used.
	incb	xms_handle_used(%bx)
	jmp	4b //@@perfect_fit

7:	//@@no_handles_left:
	mov	$XMS_NO_HANDLE_LEFT, %bl
	
8:	//@@failure_epilogue:	
	stc
	ret

//-----------------------------------------------------------------------------
// allocates an (X)XMS block
// In:	AH=89h
//	EDX=amount of XMS being requested in kbytes
// Out:	AX=1 if successful
//	  DX=handle
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> no free handles left

xms_alloc_any_xms:
	call	alloc_xms
	jc	1f // Failure.
	ret
1:
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// returns (X)XMS handle information
// In:	AH=8eh
//	DX=XMS block handle
// Out:	CY=0 if successful
//	  BH=block's lock count
//	  CX=number of free XMS handles
//	  EDX=block's length in KiB
//	CY=1 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid

get_handle_info:
	call 	xms_handle_valid
	jc	3f //@@not_valid

	push	%dx			// Save handle for later.

	xor	%ax, %ax		// ax is number of free handles.

	// Setup for loop.
	mov	$driver_end, %bx
	mov	xms_num_handles, %cx
	
1:	//@@look_again:
	cmpb	$0, xms_handle_used(%bx)	// In use?
	jne	2f //@@add_some		// Yes, go to next.
	inc	%ax			// No, one more free handle.

2: 	//@@add_some:
	add	$SIZE_xms_handle, %bx
	loop	1b //@@look_again

	//  Now ax contains number of free handles.
	mov	%ax, %cx		// Store number of free handles.
	
	pop 	%bx 			// Get handle saved earlier.
	mov	xms_handle_xsize(%bx), %edx	// Store block size.
	mov	xms_handle_used(%bx), %bh	// Store lock count.
	dec	%bh
	
	clc				// Success.
	ret

3: 	//@@not_valid:	
// Set from cmp:	stc
	ret

//-----------------------------------------------------------------------------
// returns (X)XMS handle information
// In:	AH=8eh
//	DX=XMS block handle
// Out:	AX=1 if successful
//	  BH=block's lock count
//	  CX=number of free XMS handles
//	  EDX=block's length in KiB
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a2h -> handle is invalid

xms_get_extended_handle_info:
	call 	get_handle_info
	jc	1f // Failure.

	mov	%bh, C_BH
	mov	%cx, C_CX
	mov	%edx, C_EDX
	ret
1:
	pop	%ax
	ret

//-----------------------------------------------------------------------------
// reallocates an XMS block. only supports shrinking.
// In:	AH=8fh
//	EBX=new size for the XMS block in kbytes
//	DX=unlocked XMS handle
// Out:	CY=0 if successful
//	CY=1 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> all handles are in use
//	  BL=0a2h -> invalid handle
//	  BL=0abh -> block is locked

realloc_xms:
	call	xms_handle_valid
	jc	4f //@@xms_realloc_not_valid
	
	xchg	%ebx, %edx
	mov	xms_handle_xsize(%bx), %eax
	cmp	%eax, %edx
	jbe	2f //@@shrink_it

1:	//@@no_xms_handles_left:
	mov	$XMS_NO_HANDLE_LEFT, %bl	// simulate a "no handle" error
4:	//@@xms_realloc_not_valid:	
	stc
	ret

2:	//@@shrink_it:
	mov	%bx, %si
	call	xms_find_free_handle	// Get a blank handle.
	jc	1b //@@no_xms_handles_left		// Return if there's an error.
	
	mov	%edx, xms_handle_xsize(%si)
	
	sub	%edx, %eax		// Calculate what's left over.
	jz	3f //@@dont_need_handle	// Skip if we don't need it.
	
	add	xms_handle_xbase(%si), %edx	// Calculate new base address.
	mov	%edx, xms_handle_xbase(%bx)	// Store it.
	mov	%eax, xms_handle_xsize(%bx)	// Store size.
	movb	$0, xms_handle_used(%bx)	// Block is not locked or used.
	
3:	//@@dont_need_handle:
	clc
	ret

//-----------------------------------------------------------------------------
// reallocates an XMS block. only supports shrinking.
// In:	AH=8fh
//	EBX=new size for the XMS block in kbytes
//	DX=unlocked XMS handle
// Out:	AX=1 if successful
//	AX=0 if not successful
//	  BL=080h -> function not implemented
//	  BL=081h -> VDISK is detected
//	  BL=0a0h -> all XMS is allocated
//	  BL=0a1h -> all handles are in use
//	  BL=0a2h -> invalid handle
//	  BL=0abh -> block is locked

xms_realloc_any_xms:
	call	realloc_xms
	jc	1f //@@xms_realloc_not_valid
	ret

1:	//@@xms_realloc_not_valid:	
	pop	%ax
	ret
