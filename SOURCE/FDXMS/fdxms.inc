// include file for the Free-DOS XMS driver
// Copyright 2001-2005, Martin Str�mberg.
// Copyright (c) 1995, Till Gerken

#ifndef XXMS
#define INFO_STR 	"80386 64MB version, no XXMS support"
#else
#define INFO_STR	"80386 4GB version"
#endif // XXMS

#define DRIVER_VERSION	"0.94.Bananas"
#define DRIVER_VER	94

#ifndef XXMS
#define INTERFACE_VERSION	"2.0"
#define INTERFACE_VER 		0x200
#else
#define INTERFACE_VERSION	"3.0"
#define INTERFACE_VER		0x300
#endif

#define XMS_START	1088	// XMS starts at 1088k. After HMA.
#define XMS_MAX 	0xfbc0	// The maximum amount of XMS memory.
#define INT15_MAX	XMS_MAX-1	// The maximum amount of INT15 reserved memory.
#define INT15_MAX_STR	"0xfbbf"
#define NUM_HANDLES_MAX	1024

#define CMD_INIT	0	// Init command (used when installed).
#define CMD_ISTATUS	6	// Input status command (used when checking if this is a (character?) device).
#define CMD_OSTATUS	0xa	// Output status command (used when checking if this is a (character?) device).

#define STATUS_DONE	0x0100	// Driver is done.
#define STATUS_ERROR	0x8000	// Driver failure.
#define ERROR_BAD_CMD	0x0003	// Unknown command.

#define FLAG_INITIALISED	0x1
#define FLAG_HMA_USED		0x2
#define FLAG_NO_VDISK		0x4
#define FLAG_A20_STATE		0x8

// Offsets with %bp to client registers.
#define C_AX	32(%bp)
#define C_EAX	32(%bp)
#define C_BH	21(%bp)
#define C_BL	20(%bp)
#define C_BX	20(%bp)
#define C_EBX	20(%bp)
#define C_CX	28(%bp)
#define C_ECX	28(%bp)
#define C_DX	24(%bp)
#define C_EDX	24(%bp)
#define C_SI	8(%bp)

// XMS error codes.
#define XMS_NOT_IMPLEMENTED		0x80
#define XMS_VDISK_DETECTED		0x81
#define XMS_A20_FAILURE			0x82
#define XMS_DRIVER_FAILURE		0x8e
#define XMS_DRIVER_FATAL		0x8f
#define XMS_HMA_NOT_THERE		0x90
#define XMS_HMA_IN_USE			0x91
#define XMS_HMAREQ_TOO_SMALL		0x92
#define XMS_HMA_NOT_USED		0x93
#define XMS_A20_STILL_ENABLED		0x94
#define XMS_ALREADY_ALLOCATED		0xa0
#define XMS_NO_HANDLE_LEFT		0xa1
#define XMS_INVALID_HANDLE		0xa2
#define XMS_INVALID_SOURCE_HANDLE 	0xa3
#define XMS_INVALID_SOURCE_OFFSET 	0xa4
#define XMS_INVALID_DESTINATION_HANDLE	0xa5
#define XMS_INVALID_DESTINATION_OFFSET	0xa6
#define XMS_INVALID_LENGTH		0xa7
#define XMS_OVERLAP			0xa8
#define XMS_PARITY_ERROR		0xa9
#define XMS_BLOCK_NOT_LOCKED		0xaa
#define XMS_BLOCK_LOCKED		0xab
#define XMS_LOCK_COUNT_OVERFLOW		0xac
#define XMS_LOCK_FAILED			0xad
#define XMS_ONLY_SMALLER_UMB		0xb0
#define XMS_NO_UMB_AVAILABLE		0xb1
#define XMS_UMB_SEGMENT_NR_INVALID	0xb2

#define XMS_COMMAND_LINE_LENGTH_MAX	0x100


#define request_hdr_req_size
	/* 1 */
#define request_hdr_unit_id	1
	/* 1 */
#define request_hdr_cmd		2
	/* 1 */
#define request_hdr_status	3
	/* 2 */
#define request_hdr_rsvd	5
	/* 8 */
#define SIZE_request_hdr	13


#define	init_strc_init_hdr
	/* SIZE_request_hdr */
#define	init_strc_units		SIZE_request_hdr
	/* 1 */
#define	init_strc_end_addr	SIZE_request_hdr+1
	/* 4 */
#define	init_strc_cmd_line	SIZE_request_hdr+5
	/* 4 */
#define	SIZE_init_strc		SIZE_request_hdr+9


#define desc_limit
	/* 2 */
#define desc_base0_15	2
	/* 2 */
#define desc_base16_23	4
	/* 1 */
#define	desc_type	5
	/* 1 */
#define desc_flags	
	/* 1 */
#define desc_base24_31	6
	/* 1 */
#define SZIE_desc	8


#define xms_move_strc_len
	/* 4 */
#define xms_move_strc_src_handle	4
	/* 2 */
#define xms_move_strc_src_offset	6
	/* 4 */
#define xms_move_strc_dest_handle	10
	/* 2 */
#define xms_move_strc_dest_offset	12
	/* 4 */
#define SIZE_xms_move_strc		16


#ifndef XXMS

#define xms_handle_xsize
	/* 2 */
#define xms_handle_xbase 	2
	/* 2 */
#define xms_handle_used 	4
	/* 1 */
#define SIZE_xms_handle 	5

#else

#define xms_handle_xsize
        /* 4 */
#define xms_handle_xbase 	4
        /* 4 */
#define xms_handle_used 	8
        /* 1 */
#define SIZE_xms_handle 	9

#define smap_strc_address
	/* 4 */
#define smap_strc_address_high 	4
	/* 4 */
#define smap_strc_length 	8
	/* 4 */
#define smap_strc_length_high 	12
	/* 4 */
#define smap_strc_type 		16
	/* 4 */
#define SIZE_smap_strc		20

#define SMAP_MEMORY		1

#endif

