// Copyright 2001-2005, Martin Str�mberg.
//-----------------------------------------------------------------------------
// Tries to find memory by INT15 EAX=0xe820.
// In:	DS - initialized with code segment
//	SI - pointing at start of handle table
// Out:	EAX - total amount of memory found in kiB.
//	SI - pointing at next entry to be filled in in handle table
//	EBX - destroyed.
//	ECX - destroyed.
//	EDX - destroyed.
//	DI - destroyed.
	
xms_size:		
	.long	0	// Size of XMS in KiB.
	
smap_buf:		
	.space	SIZE_smap_strc, 0
smap_high_address:
	.ascii	"Base address higher than 0xffffffff detected. This release of FDXMS does not\r\n"
	.ascii	"support this. Ignoring this address range.\r\n$"
smap_high_length:
	.ascii	"Length longer than 0xffef0000 detected. This release of FDXMS does not\r\n"
	.ascii	"support this. Clamping length.\r\n$"
smap_too_long:
	.ascii	"Range length crossing 0xffffffff detected. Clamping length.\r\n$"

too_few_handles_pre:
	.ascii	"Due to memory configuration the number of handles has been increased to $"
too_few_handles_post:
	.byte	13, 10, '$'

int15ax0xe820:
	push	%es
	mov	%cs, %ax
	mov	%ax, %es
	mov	$smap_buf, %di
	xor	%ebx, %ebx
	mov	$(((('S'*256+'M')*256)+'A')*256+'P'), %edx

	mov	$SIZE_smap_strc, %ecx
	mov	$0xe820, %eax
	push    %esi			// At least one BIOS clobbers ESI.
	int	$0x15
	pop	%esi
	jc	9f //@@smap_end
	cmp	$(((('S'*256+'M')*256)+'A')*256+'P'), %eax
	jne	9f //@@smap_end
	cmp	$SIZE_smap_strc, %ecx
	jne	9f //@@smap_end

1:	//@@smap_found:	
	cmpl	$SMAP_MEMORY, smap_strc_type(%di)
	jne	8f //@@next_smap

	// Upp the address to even KiB.
	mov	smap_strc_address(%di), %eax
	and	$0x3ff, %eax
	jz	2f //@@check_high_address
	mov	$0x400, %ecx
	sub	%eax, %ecx
	sub	%ecx, smap_strc_length(%di)
	sbbl	$0, smap_strc_length_high(%di)
	jc	8f //@@next_smap	// If we have no length left, ignore this block.
	addl	$0x3ff,smap_strc_address(%di)
	adcl	$0, smap_strc_address_high(%di)
	andl	$0xfffffc00, smap_strc_address(%di)

2:	//@@check_high_address:	
	cmpl	$0, smap_strc_address_high(%di)	// Any high bits set?
	je	3f //@@no_high_address

	mov	%dx, %cx		// Save dx.
	mov	$smap_high_address, %dx
	mov	$9, %ah
	int	$0x21			// Output appropiate complaintive message.
	mov	%cx, %dx		// Restore dx.
	jmp	8f //@@next_smap	// Ignore this block.
	
3:	//@@no_high_address:	
	mov	smap_strc_address(%di), %eax
	cmp	$0x100000, %eax		// Not XMS memory?
	jb	8f //@@next_smap
	
	sub	$0x110000, %eax
	jae	4f //@@not_hma
	
	neg	%eax			// Reserve HMA.
	add	%eax, smap_strc_address(%di)
	sub	%eax, smap_strc_length(%di)
	jnc	4f //@@not_hma	// If the length was smaller than 64KiB,
	jmp	8f //@@next_smap	// we ignore this memory block.

4:	//@@not_hma:
	cmpl	$0, smap_strc_length_high(%di)		// Any high bits set?
	je	5f //@@no_high_length

	mov	%dx, %cx		// Save dx.
	mov	$smap_high_length, %dx
	mov	$9, %ah
	int	$0x21			// Output appropiate complaintive message.
	mov	%cx, %dx		// Restore dx.
	movl	$0, smap_strc_length_high(%di)		// Zero it out
	movl	$0xffef0000, smap_strc_length(%di)	// and set max size.

5:	//@@no_high_length:	
	mov	smap_strc_address(%di), %eax
	mov	%eax, %ecx		// Store start address in ECX.
	
	dec	%eax				// Verify that end address
	add	smap_strc_length(%di), %eax	// is not bigger than 
	jnc	6f //@@end_address_ok		// 0xffffffff.

	mov	$smap_too_long, %dx
	mov	$9, %ah
	int	$0x21

	xor	%eax, %eax		// Update length so end address is 0xffffffff.
	sub	%ecx, %eax
	mov	%eax, smap_strc_length(%di)
	
	mov	$0xffffffff, %eax

6:	//@@end_address_ok:
	cmp	xms_highest_address, %eax	// Is this bigger than what 
	jb	7f //@@setup_one_block	// we've got so far?

	mov	%eax, xms_highest_address	// Yes, update.

7:	//@@setup_one_block:
	mov	smap_strc_length(%di), %eax
	add	%eax, xms_size		// Update found XMS memory.
	shr	$10, %eax		// Convert to KiB.
	mov	%eax, xms_handle_xsize(%si)
	shr	$10, %ecx		// Convert to KiB.
	mov	%ecx, xms_handle_xbase(%si)
	movb	$0, xms_handle_used(%si)	// Not in use.
	add	$SIZE_xms_handle, %si	// Advance handle pointer.
		
8:	//@@next_smap:
	test	%ebx, %ebx
	jz	9f //@@smap_end
	mov	$SIZE_smap_strc, %ecx
	mov	$0xe820, %eax
	push    %esi			// At least one BIOS clobbers ESI.
	int	$0x15
	pop	%esi
	jc	9f //@@smap_end
	jmp	1b //@@smap_found

9:	//@@smap_end:
	// Verify that there's at least one free handle.
	xor	%eax, %eax		// For call to print_dec_number below.
	mov	%si, %ax
	mov	%si, zero_out_region_start
	sub	$driver_end, %ax
	xor	%dx, %dx
	mov	$SIZE_xms_handle, %cx
	div	%cx
	cmp	xms_num_handles, %ax
	jb	10f //@@done

	// We need to increase the number of handles.
	inc	%ax
	mov	%ax, xms_num_handles
	mov	%ax, %bx
	// Tell the user that we did.
	mov	$9, %ah
	mov	$too_few_handles_pre, %dx
	int	$0x21
	mov	%bx, %ax
	call	print_dec_number
	mov	$9, %ah
	mov	$too_few_handles_post, %dx
	int	$0x21

10:	//@@done:
	pop	%es
	mov	xms_size, %eax		// Return amount of memory found in kiB in EAX.

	ret
